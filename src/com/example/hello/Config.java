package com.example.hello;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class Config {

    WebDriver driver;
    
	@BeforeTest
	public void beforeTest() {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/chromedriver.exe");
		driver = new ChromeDriver();
	}
	
	@AfterTest(alwaysRun = true)
	public void afterTest() {
		driver.close();
        driver.quit();
	}
}

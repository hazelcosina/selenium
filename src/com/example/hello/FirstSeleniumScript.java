package com.example.hello;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

public class FirstSeleniumScript extends Config{

	
	@Test
	public void sampple() {

        driver.get("http://newtours.demoaut.com/");
        System.out.println("Page title is: " + driver.getTitle());
        
        WebElement element = driver.findElement(By.xpath("//a[contains(text(),'Register')]"));
        element.click();

        WebElement firstName = driver.findElement(By.name("firstName"));
        firstName.sendKeys("Hazel");
        
        WebElement lastName = driver.findElement(By.name("lastName"));
        lastName.sendKeys("Cosina");
        
        WebElement phone = driver.findElement(By.name("phone"));
        phone.sendKeys("09472035166");
        
        WebElement email = driver.findElement(By.name("userName"));
        email.sendKeys("hazelcosina2@gmail.com");
        
        WebElement addressOne = driver.findElement(By.name("address1"));
        addressOne.sendKeys("Guibilondo St.");
        
        WebElement addressTwo = driver.findElement(By.name("address2"));
        addressTwo.sendKeys("Mabolo");
        
        WebElement city = driver.findElement(By.name("city"));
        city.sendKeys("Cebu City");
        
        WebElement state = driver.findElement(By.name("state"));
        state.sendKeys("Cebu");
        
        WebElement postal = driver.findElement(By.name("postalCode"));
        postal.sendKeys("6000");
        
        Select country = new Select(driver.findElement(By.name("country")));
        country.selectByVisibleText("PHILIPPINES");
        
        WebElement userName = driver.findElement(By.id("email"));
        userName.sendKeys("hazelcosina2@gmail.com");
        
        WebElement password = driver.findElement(By.name("password"));
        password.sendKeys("cosina321");
        
        WebElement confirmPassword = driver.findElement(By.name("confirmPassword"));
        confirmPassword.sendKeys("cosina321");

        System.out.println("");
        WebElement submit = driver.findElement(By.xpath("//input[@name='register']"));
        submit.click();
        
        //Sign In Page

        WebElement signin = driver.findElement(By.xpath("//a[@href='mercurysignon.php']"));
        signin.click();
        
        WebElement loginUsername = driver.findElement(By.name("userName"));
        loginUsername.sendKeys("hazelcosina@gmail.com");
        
        WebElement loginPassword = driver.findElement(By.name("password"));
        loginPassword.sendKeys("cosina321");
        
        WebElement login = driver.findElement(By.name("login"));
        login.click();
       
	}

}
